<?php
/**
 * Name:
 * User: Kitty.Cheng
 * Mail: kitty.cheng@18php.com
 * Date: 2020/3/20
 * Time: 下午4:30
 * Created by 18php.com
 */

return [
    'version' => '1.0',
    'domain' => '',

    'state' => [
        '2' => '已停用',
        '1' => '已启用',
    ],
    'state_show' => [
        '2' => '不显示',
        '1' => '显示',
    ],
    'is_state' => [
        '2' => '否',
        '1' => '是',
    ],

    'is_recommend' => [
        '0' => '未推荐',
        '1' => '已推荐',
    ],

    'un_is_recommend' => [
        '0' => '推荐',
        '1' => '取消推荐',
    ],

    'web_lang' => [
        'zh-cn' => 'cn',
        'en-us' => 'en',
    ],

    'web_lang_name' => [
        'cn' => '中文',
        'en' => 'English',
    ],

    'default_lang' => env('lang.default_lang', 'zh-cn'),
];
