/*
MySQL Backup
Database: k-cms
Backup Time: 2022-09-13 03:59:38
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `k-cms`.`k_admin`;
DROP TABLE IF EXISTS `k-cms`.`k_article`;
DROP TABLE IF EXISTS `k-cms`.`k_article_cate`;
DROP TABLE IF EXISTS `k-cms`.`k_category`;
DROP TABLE IF EXISTS `k-cms`.`k_company`;
DROP TABLE IF EXISTS `k-cms`.`k_company_cate`;
DROP TABLE IF EXISTS `k-cms`.`k_link`;
DROP TABLE IF EXISTS `k-cms`.`k_site_config`;
CREATE TABLE `k_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_name` char(50) NOT NULL DEFAULT '' COMMENT '登录用户名',
  `password` char(50) NOT NULL DEFAULT '' COMMENT '登录密码',
  `mobile` char(11) NOT NULL DEFAULT '' COMMENT '手机号',
  `email` char(50) NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `nickname` char(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `note` char(50) NOT NULL DEFAULT '' COMMENT '备注信息',
  `login_ip` char(50) NOT NULL DEFAULT '' COMMENT '登录ip',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '启用状态,1启用,0禁用',
  `login_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='后台管理员';
CREATE TABLE `k_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `rank` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-普通 2-推荐 3-精品 4-热门',
  `is_top` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-默认 2-置顶',
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-不显示 1-显示',
  `author` char(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '作者',
  `describe` char(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述',
  `details` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '详情',
  `img` char(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片',
  `clicks` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击数',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序，越小越优先',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
  `article_cate_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文章分类id',
  `lang_tag` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cn' COMMENT '语言标识',
  `delete_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文章表';
CREATE TABLE `k_article_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cate_name` char(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `describe` char(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `update_time` datetime DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `lang_tag` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cn' COMMENT '语言标识',
  PRIMARY KEY (`id`),
  KEY `title` (`cate_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文章分类表';
CREATE TABLE `k_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '栏目标题',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT 'url',
  `seo_url` varchar(200) NOT NULL DEFAULT '' COMMENT 'seo_url',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '10' COMMENT '排序',
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '显示状态，1显示，2不显示',
  `target` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '跳转方式，1-self默认，2_blank',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `level` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '栏目级别',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='栏目管理';
CREATE TABLE `k_company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `rank` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-普通 2-推荐 3-精品 4-热门',
  `is_top` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-默认 2-置顶',
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-不显示 1-显示',
  `author` char(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '作者',
  `describe` char(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述',
  `details` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '详情',
  `img` char(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片',
  `clicks` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击数',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序，越小越优先',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
  `article_cate_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文章分类id',
  `lang_tag` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cn' COMMENT '语言标识',
  `delete_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='公司信息';
CREATE TABLE `k_company_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cate_name` char(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `describe` char(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `update_time` datetime DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `lang_tag` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cn' COMMENT '语言标识',
  PRIMARY KEY (`id`),
  KEY `title` (`cate_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文章分类表';
CREATE TABLE `k_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `description` char(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `url` char(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `img` char(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `sort` int(10) NOT NULL DEFAULT '1' COMMENT '标题',
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `delete_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='友情链接';
CREATE TABLE `k_site_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `site_title` varchar(255) NOT NULL DEFAULT '' COMMENT '站点标题名称',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT 'seo关键词',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT 'seo描述',
  `copyright` varchar(100) NOT NULL DEFAULT '' COMMENT '底部版权信息',
  `icp` varchar(100) NOT NULL DEFAULT '' COMMENT 'icp备案信息',
  `tel` varchar(200) NOT NULL DEFAULT '' COMMENT '电话',
  `mobile` varchar(200) NOT NULL DEFAULT '' COMMENT '手机号',
  `hot_tel` varchar(200) NOT NULL DEFAULT '' COMMENT '热线电话',
  `qq` varchar(20) NOT NULL DEFAULT '' COMMENT 'qq',
  `weixin` varchar(50) NOT NULL DEFAULT '' COMMENT '微信',
  `github` varchar(200) NOT NULL DEFAULT '' COMMENT 'github',
  `gitee` varchar(200) NOT NULL DEFAULT '' COMMENT 'gitee',
  `email` varchar(200) NOT NULL DEFAULT '' COMMENT '邮箱',
  `fax` varchar(200) NOT NULL DEFAULT '' COMMENT '传真',
  `address` varchar(200) NOT NULL DEFAULT '' COMMENT '公司地址',
  `logo` varchar(200) NOT NULL DEFAULT '' COMMENT 'logo图片',
  `code2` varchar(200) NOT NULL DEFAULT '' COMMENT '二维码',
  `script_code` text NOT NULL COMMENT '统计代码',
  `smtp_host` varchar(100) NOT NULL DEFAULT '' COMMENT 'smtp服务器地址',
  `smtp_port` varchar(10) NOT NULL DEFAULT '' COMMENT 'smtp端口',
  `email_account` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱账号，发信',
  `email_pwd` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱密码',
  `email_recipient` varchar(200) NOT NULL DEFAULT '' COMMENT '收件人邮箱',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `page_list_rows` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '分页数量，默认10',
  `theme` varchar(50) NOT NULL DEFAULT 'default' COMMENT '模板主题目录',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='网站配置信息';
BEGIN;
LOCK TABLES `k-cms`.`k_admin` WRITE;
DELETE FROM `k-cms`.`k_admin`;
INSERT INTO `k-cms`.`k_admin` (`id`,`user_name`,`password`,`mobile`,`email`,`nickname`,`note`,`login_ip`,`state`,`login_count`,`login_time`,`update_time`,`create_time`,`delete_time`) VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '18819009046', '450038893@qq.com', 'kitty.cheng', '超级管理员', '127.0.0.1', 1, 281, '2022-09-12 23:45:41', '2022-09-12 23:45:42', '2022-06-13 11:17:33', NULL),(2, '123', '', '13798459046', '123@qq.com', '', '', '', 1, 0, NULL, '2022-09-12 19:33:37', '2022-09-12 19:08:11', NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `k-cms`.`k_article` WRITE;
DELETE FROM `k-cms`.`k_article`;
INSERT INTO `k-cms`.`k_article` (`id`,`title`,`rank`,`is_top`,`state`,`author`,`describe`,`details`,`img`,`clicks`,`sort`,`admin_id`,`article_cate_id`,`lang_tag`,`delete_time`,`update_time`,`create_time`) VALUES (5, '美国海运盐田卡派专线', 0, 0, 1, '', '美国海运盐田卡派专线美国海运美森卡派专线美国海运美森卡派专线', '<h5 style=\"text-indent: 0px; text-align: left;\">美国海运盐田卡派专线</h5>', '/storage/images/20220629/2107f64119ecbde494ba95a7db2048b1.jpg', 233, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:15', '2022-06-18 00:00:00'),(6, '美国海运美森卡派专线', 0, 0, 1, '', '美国海运美森卡派专线美国海运美森卡派专线美国海运美森卡派专线', '<p><img src=\"//k-cms.test/storage/images/20220627/02d8a3fc27412f2cf2c4759011d24d7c.jpg\" alt=\"123\" data-href=\"//k-cms.test/storage/images/20220627/02d8a3fc27412f2cf2c4759011d24d7c.jpg\" style=\"\"/><img src=\"//k-cms.test/storage/images/20220629/1ff9151581c914e2a6e72604eca84378.png\" alt=\"123\" data-href=\"//k-cms.test/storage/images/20220629/1ff9151581c914e2a6e72604eca84378.png\" style=\"\"/></p>', '/storage/images/20220629/51b27b7a6e46f26c362b2976cbc341cc.jpg', 211, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:15', '2022-06-18 00:00:00'),(7, '美国海运美森专线', 1, 1, 1, '', '美国海运美森专线美国海运美森专线美国海运美森专线美国海运美森专线', '<p><span style=\"color: rgb(33, 37, 41); background-color: rgb(255, 255, 255); font-size: 16px;\">美国海运美森专线美国海运美森专线美国海运美森专线</span></p>', '/storage/images/20220629/ee59cd0aad0bb257ddc5046487af1b3b.jpg', 0, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:14', '2022-06-29 01:16:30'),(8, '美国海运盐田专线', 1, 1, 1, '', '美国海运盐田专线美国海运美森卡派专线美国海运美森卡派专线', '<h5 style=\"text-indent: 0px; text-align: left;\">美国海运盐田专线</h5><p><img src=\"//k-cms.test/storage/images/20220629/04d4d03289738407e68e4e4751407a48.png\" alt=\"123\" data-href=\"//k-cms.test/storage/images/20220629/04d4d03289738407e68e4e4751407a48.png\" style=\"\"/></p>', '/storage/images/20220629/bd627cf31d495cab4dc67ef48f935567.jpg', 0, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:14', '2022-06-29 01:18:18'),(9, '美国海运盐田专线', 1, 1, 1, '', '美国海运盐田专线美国海运美森卡派专线美国海运美森卡派专线', '<h5 style=\"text-indent: 0px; text-align: left;\">美国海运盐田专线</h5><p><img src=\"//k-cms.test/storage/images/20220629/04d4d03289738407e68e4e4751407a48.png\" alt=\"123\" data-href=\"//k-cms.test/storage/images/20220629/04d4d03289738407e68e4e4751407a48.png\" style=\"\"/></p>', '/storage/images/20220629/bd627cf31d495cab4dc67ef48f935567.jpg', 0, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:14', '2022-06-29 01:18:18'),(10, '公司简介', 1, 1, 1, '', '<p>深圳八方跨境供应链管理有限公司成立于2019年12月30日，公司位于深圳市宝安福永，公司致力于国际物流，供应链管理，电子商务，为工厂和平台客户提供专业的供应链方案，将他们的产品安全、快捷送达。</p>\n<p>经过数年的发展，八方跨境供应链已经拥有自主的美国专线渠道，经营业务包含海运订舱，国内拖车，出口报关报检，进口清关和派送服务，可以分段操作或者一站式服务，并不断整合自身和社会物流资源，进一步优化运营成本，为客户提供全方位物流及个性化服务，实现共享共赢！</p>', '<p style=\"text-indent: 2.5rem; text-align: left;\">深圳八方跨境供应链管理有限公司成立于2019年12月30日，公司位于深圳市宝安福永，公司致力于国际物流，供应链管理，电子商务，为工厂和平台客户提供专业的供应链方案，将他们的产品安全、快捷送达。</p><p style=\"text-indent: 2.5rem; text-align: left;\">经过数年的发展，八方跨境供应链已经拥有自主的美国专线渠道，经营业务包含海运订舱，国内拖车，出口报关报检，进口清关和派送服务，可以分段操作或者一站式服务，并不断整合自身和社会物流资源，进一步优化运营成本，为客户提供全方位物流及个性化服务，实现共享共赢！</p><p style=\"text-indent: 2.5rem; text-align: left;\"><br/></p><p style=\"text-indent: 2.5rem; text-align: left;\"><img src=\"http://k-cms.test/storage/images/20220629/e0a4974d4cc0426589ba8db59e42d359.jpg\"/></p>', '/storage/images/20220629/e0a4974d4cc0426589ba8db59e42d359.jpg', 0, 10, 0, '5', 'cn', NULL, '2022-09-13 03:38:21', '2022-06-29 01:30:46'),(11, '联系我们', 1, 1, 1, '', '联系我们', '<p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; text-indent: 20px; color: rgb(51, 51, 51); font-family: 微软雅黑, 新宋体, simsun; white-space: normal; text-align: center;\"><br/></p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; text-indent: 20px; color: rgb(51, 51, 51); font-family: 微软雅黑, 新宋体, simsun; white-space: normal;\"><br/></p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; text-indent: 20px; color: rgb(51, 51, 51); font-family: 微软雅黑, 新宋体, simsun; white-space: normal;\"><br/></p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; text-indent: 20px; color: rgb(51, 51, 51); font-family: 微软雅黑, 新宋体, simsun; white-space: normal;\"><br/></p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; text-indent: 20px; color: rgb(51, 51, 51); font-family: 微软雅黑, 新宋体, simsun; white-space: normal; text-align: center;\">更新时间：2020-05-06</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; text-indent: 20px; color: rgb(51, 51, 51); font-family: 微软雅黑, 新宋体, simsun; white-space: normal;\"><br/></p><table cellpadding=\"0\" cellspacing=\"0\" height=\"302\" align=\"center\"><colgroup><col width=\"172\" span=\"5\" style=\"width: 129pt;\"/></colgroup><tbody><tr height=\"64\" class=\"firstRow\" style=\"border: 1px solid rgb(0, 0, 0); height: 48pt;\"><td colspan=\"5\" class=\"et3\" height=\"48\" width=\"180\" x:str=\"\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\"><span style=\"font-size: 20px;\"><strong>八方安运对外联络通讯录</strong></span></td></tr><tr height=\"34\" style=\"border: 1px solid rgb(0, 0, 0); height: 25.5pt;\"><td colspan=\"2\" class=\"et4\" height=\"25\" x:str=\"\" align=\"center\" valign=\"middle\" width=\"180\" style=\"border-color: rgb(0, 0, 0);\"><strong>部门</strong></td><td class=\"et5\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\"><strong>座机</strong></td><td class=\"et5\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\"><strong>手机</strong></td><td class=\"et5\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\"><strong>邮箱</strong></td></tr><tr height=\"34\" style=\"border: 1px solid rgb(0, 0, 0); height: 25.5pt;\"><td rowspan=\"3\" class=\"et4\" height=\"76\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\">运营</td><td class=\"et6\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\">港前客服</td><td class=\"et6\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\">0755-29190096</td><td rowspan=\"2\" class=\"et5\" x:num=\"18126255091\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\">-</td><td class=\"et7\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\"><a href=\"mailto:cust2@8fay.com\" style=\"padding: 0px; margin: 0px; text-decoration-line: none; color: rgb(51, 51, 51); display: block;\">cust4@8fay.com</a></td></tr><tr height=\"34\" style=\"border: 1px solid rgb(0, 0, 0); height: 25.5pt;\"><td class=\"et6\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\">港后客服</td><td class=\"et6\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\">0755-29190096</td><td class=\"et7\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\"><a href=\"mailto:cust3@8fay.com\" style=\"padding: 0px; margin: 0px; text-decoration-line: none; color: rgb(51, 51, 51); display: block;\">cust2@8fay.com</a></td></tr><tr height=\"34\" style=\"border: 1px solid rgb(0, 0, 0); height: 25.5pt;\"><td class=\"et5\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\">财务</td><td class=\"et6\" x:str=\"\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\">0755-29190096</td><td class=\"et5\" x:num=\"15820112459\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\">15820112459</td><td class=\"et7\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\"><a href=\"mailto:exacc@8fay.com\" style=\"padding: 0px; margin: 0px; text-decoration-line: none; color: rgb(51, 51, 51); display: block;\">exacc@8fay.com</a></td></tr><tr height=\"34\" style=\"border: 1px solid rgb(0, 0, 0); height: 25.5pt;\"><td colspan=\"2\" class=\"et4\" height=\"25\" x:str=\"\" align=\"center\" valign=\"middle\" width=\"180\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\">操作</td><td class=\"et6\" width=\"180\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\">-</td><td class=\"et5\" x:num=\"13688841167\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\">13688841167</td><td class=\"et7\" width=\"180\" align=\"center\" valign=\"middle\" style=\"border-color: rgb(0, 0, 0);\"><a href=\"mailto:oper1@8fay.com\" style=\"padding: 0px; margin: 0px; text-decoration-line: none; color: rgb(51, 51, 51); display: block;\">oper1@8fay.com</a></td></tr><tr height=\"34\" style=\"border: 1px solid rgb(0, 0, 0); height: 25.5pt;\"><td colspan=\"5\" class=\"et5\" height=\"25\" x:str=\"\" width=\"180\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\">海外集运咨询： cust@8fay.com</td></tr><tr height=\"34\" style=\"border: 1px solid rgb(0, 0, 0); height: 25.5pt;\"><td colspan=\"5\" class=\"et5\" height=\"25\" x:str=\"\" width=\"180\" style=\"border-color: rgb(0, 0, 0); word-break: break-all;\">投诉电话：13332997997，邮箱 andy@8fay.com<br/><br/></td></tr></tbody></table><p><br/></p>', '/storage/images/20220913/0a6fed71f19377b1da58d33a1371fc3c.png', 0, 0, 0, '4', 'cn', NULL, '2022-09-13 03:08:20', '2022-09-13 02:50:11'),(12, '客服招聘', 1, 1, 1, '', '客服招聘', '<p>客服招聘客服招聘客服招聘客服招聘客服招聘客服招聘</p>', '/storage/images/20220913/8080c0134ea7aced4d2f4ddef52adfb5.png', 0, 0, 0, '6', 'cn', NULL, '2022-09-13 03:08:18', '2022-09-13 03:05:15'),(13, '助力香港抗击疫情', 1, 1, 1, '', '助力香港抗击疫情', '<p>助力香港抗击疫情助力香港抗击疫情助力香港抗击疫情</p><p><br/></p><p><br/></p><p>助力香港抗击疫情</p><p><br/></p><p><br/></p><p>助力香港抗击疫情</p>', '/storage/images/20220913/792ed020011c82de5e255cf2cff878c4.png', 0, 0, 0, '2', 'cn', NULL, '2022-09-13 03:27:25', '2022-09-13 03:27:25'),(14, '助力香港抗击疫情1', 1, 1, 1, '', '助力香港抗击疫情', '<p>助力香港抗击疫情助力香港抗击疫情助力香港抗击疫情</p><p><br/></p><p><br/></p><p>助力香港抗击疫情</p><p><br/></p><p><br/></p><p>助力香港抗击疫情</p>', '/storage/images/20220913/792ed020011c82de5e255cf2cff878c4.png', 0, 0, 0, '2', 'cn', NULL, '2022-09-13 03:27:25', '2022-09-13 03:27:25'),(15, '助力香港抗击疫情1', 1, 1, 1, '', '助力香港抗击疫情', '<p>助力香港抗击疫情助力香港抗击疫情助力香港抗击疫情</p><p><br/></p><p><br/></p><p>助力香港抗击疫情</p><p><br/></p><p><br/></p><p>助力香港抗击疫情</p>', '/storage/images/20220913/792ed020011c82de5e255cf2cff878c4.png', 0, 0, 0, '2', 'cn', NULL, '2022-09-13 03:27:25', '2022-09-13 03:27:25'),(16, '助力香港抗击疫情1', 1, 1, 1, '', '助力香港抗击疫情', '<p>助力香港抗击疫情助力香港抗击疫情助力香港抗击疫情</p><p><br/></p><p><br/></p><p>助力香港抗击疫情</p><p><br/></p><p><br/></p><p>助力香港抗击疫情</p>', '/storage/images/20220913/792ed020011c82de5e255cf2cff878c4.png', 0, 0, 0, '2', 'cn', NULL, '2022-09-13 03:27:25', '2022-09-13 03:27:25');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `k-cms`.`k_article_cate` WRITE;
DELETE FROM `k-cms`.`k_article_cate`;
INSERT INTO `k-cms`.`k_article_cate` (`id`,`cate_name`,`describe`,`state`,`sort`,`update_time`,`delete_time`,`create_time`,`lang_tag`) VALUES (1, '产品中心', '产品中心', 1, 10, '2022-09-13 02:34:38', NULL, '2022-06-26 14:41:01', 'cn'),(2, '新闻中心', '新闻中心', 1, 10, '2022-09-13 02:34:50', NULL, '2022-06-26 14:41:04', 'cn'),(3, '解决方案', '解决方案', 1, 10, '2022-09-13 02:35:11', NULL, '2022-06-26 14:41:07', 'cn'),(4, '联系我们', '联系我们', 1, 10, '2022-09-13 01:34:21', NULL, '2022-06-26 14:41:09', 'cn'),(5, '公司简介', '公司简介', 1, 0, '2022-09-13 00:26:07', NULL, '2022-06-29 01:29:34', 'cn'),(6, '加入我们', '加入我们', 1, 0, '2022-09-13 03:03:40', NULL, '2022-09-13 03:03:22', 'cn');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `k-cms`.`k_category` WRITE;
DELETE FROM `k-cms`.`k_category`;
INSERT INTO `k-cms`.`k_category` (`id`,`title`,`describe`,`url`,`seo_url`,`sort`,`state`,`target`,`pid`,`level`,`delete_time`,`update_time`,`create_time`) VALUES (1, '首页', '首页', '/', '', 1, 1, 0, 0, 0, NULL, '2022-06-10 00:00:00', '2022-06-10 00:00:00'),(2, '产品中心', '产品中心', '/cate/1.html', '', 4, 1, 0, 0, 0, NULL, '2022-06-10 00:00:00', '2022-06-10 00:00:00'),(3, '新闻中心', '', '/cate/2.html', '单独', 3, 1, 0, 0, 0, NULL, '2022-06-10 00:00:00', '2022-06-10 00:00:00'),(4, '解决方案', '解决方案', '/cate/3.html', '', 5, 1, 0, 0, 0, NULL, '2022-06-10 00:00:00', '2022-06-10 00:00:00'),(5, '关于我们', '关于我们', '/cate/5.html', '', 2, 1, 0, 0, 0, NULL, '2022-06-10 00:00:00', '2022-06-10 00:00:00'),(6, '加入我们', '加入我们', '/cate/6.html', '', 7, 1, 0, 0, 0, NULL, '2022-06-10 00:00:00', '2022-06-10 00:00:00'),(7, '联系我们', '联系我们', '/cate/4.html', '', 8, 1, 0, 0, 0, NULL, '2022-06-10 00:00:00', '2022-06-10 00:00:00'),(11, '服务与支持', '服务与支持', '/support/lst', '', 6, 2, 0, 0, 0, NULL, '2022-06-10 00:00:00', '2022-06-10 00:00:00');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `k-cms`.`k_company` WRITE;
DELETE FROM `k-cms`.`k_company`;
INSERT INTO `k-cms`.`k_company` (`id`,`title`,`rank`,`is_top`,`state`,`author`,`describe`,`details`,`img`,`clicks`,`sort`,`admin_id`,`article_cate_id`,`lang_tag`,`delete_time`,`update_time`,`create_time`) VALUES (1, '美国海运盐田卡派专线', 0, 0, 1, '', '美国海运盐田卡派专线美国海运美森卡派专线美国海运美森卡派专线', '<h5 style=\"text-indent: 0px; text-align: left;\">美国海运盐田卡派专线</h5>', '//k-cms.test/storage/images/20220629/2107f64119ecbde494ba95a7db2048b1.jpg', 233, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:15', '2022-06-18 00:00:00'),(2, '美国海运美森卡派专线', 0, 0, 1, '', '美国海运美森卡派专线美国海运美森卡派专线美国海运美森卡派专线', '<p><img src=\"//k-cms.test/storage/images/20220627/02d8a3fc27412f2cf2c4759011d24d7c.jpg\" alt=\"123\" data-href=\"//k-cms.test/storage/images/20220627/02d8a3fc27412f2cf2c4759011d24d7c.jpg\" style=\"\"/><img src=\"//k-cms.test/storage/images/20220629/1ff9151581c914e2a6e72604eca84378.png\" alt=\"123\" data-href=\"//k-cms.test/storage/images/20220629/1ff9151581c914e2a6e72604eca84378.png\" style=\"\"/></p>', '//k-cms.test/storage/images/20220629/51b27b7a6e46f26c362b2976cbc341cc.jpg', 211, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:15', '2022-06-18 00:00:00'),(3, '美国海运美森专线', 1, 1, 1, '', '美国海运美森专线美国海运美森专线美国海运美森专线美国海运美森专线', '<p><span style=\"color: rgb(33, 37, 41); background-color: rgb(255, 255, 255); font-size: 16px;\">美国海运美森专线美国海运美森专线美国海运美森专线</span></p>', '//k-cms.test/storage/images/20220629/ee59cd0aad0bb257ddc5046487af1b3b.jpg', 0, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:14', '2022-06-29 01:16:30'),(4, '美国海运盐田专线', 1, 1, 1, '', '美国海运盐田专线美国海运美森卡派专线美国海运美森卡派专线', '<h5 style=\"text-indent: 0px; text-align: left;\">美国海运盐田专线</h5><p><img src=\"//k-cms.test/storage/images/20220629/04d4d03289738407e68e4e4751407a48.png\" alt=\"123\" data-href=\"//k-cms.test/storage/images/20220629/04d4d03289738407e68e4e4751407a48.png\" style=\"\"/></p>', '//k-cms.test/storage/images/20220629/bd627cf31d495cab4dc67ef48f935567.jpg', 0, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:14', '2022-06-29 01:18:18'),(5, '美国海运盐田专线', 1, 1, 1, '', '美国海运盐田专线美国海运美森卡派专线美国海运美森卡派专线', '<h5 style=\"text-indent: 0px; text-align: left;\">美国海运盐田专线</h5><p><img src=\"//k-cms.test/storage/images/20220629/04d4d03289738407e68e4e4751407a48.png\" alt=\"123\" data-href=\"//k-cms.test/storage/images/20220629/04d4d03289738407e68e4e4751407a48.png\" style=\"\"/></p>', '//k-cms.test/storage/images/20220629/bd627cf31d495cab4dc67ef48f935567.jpg', 0, 10, 0, '1', 'cn', NULL, '2022-09-12 19:47:14', '2022-06-29 01:18:18'),(6, '公司简介', 1, 1, 1, '', '公司简介', '<p style=\"text-indent: 2.5rem; text-align: left;\">深圳八方跨境供应链管理有限公司成立于2019年12月30日，公司位于深圳市宝安福永，公司致力于国际物流，供应链管理，电子商务，为工厂和平台客户提供专业的供应链方案，将他们的产品安全、快捷送达。</p><p style=\"text-indent: 2.5rem; text-align: left;\">经过数年的发展，八方跨境供应链已经拥有自主的美国专线渠道，经营业务包含海运订舱，国内拖车，出口报关报检，进口清关和派送服务，可以分段操作或者一站式服务，并不断整合自身和社会物流资源，进一步优化运营成本，为客户提供全方位物流及个性化服务，实现共享共赢！</p>', '//k-cms.test/storage/images/20220629/e0a4974d4cc0426589ba8db59e42d359.jpg', 0, 10, 0, '1', 'cn', NULL, '2022-09-13 00:18:27', '2022-06-29 01:30:46');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `k-cms`.`k_company_cate` WRITE;
DELETE FROM `k-cms`.`k_company_cate`;
INSERT INTO `k-cms`.`k_company_cate` (`id`,`cate_name`,`describe`,`state`,`sort`,`update_time`,`delete_time`,`create_time`,`lang_tag`) VALUES (1, '公司简介', '公司简介', 1, 10, '2022-09-13 01:20:51', NULL, '2022-06-26 14:41:01', 'cn'),(2, '联系我们', '联系我们', 1, 10, '2022-09-13 01:25:01', NULL, '2022-06-26 14:41:04', 'cn'),(3, '服务与支持', '服务与支持1', 1, 10, '2022-09-13 01:25:06', '2022-09-13 01:25:06', '2022-06-26 14:41:07', 'cn'),(4, '关于我们', '关于我们', 1, 10, '2022-09-13 01:25:09', '2022-09-13 01:25:09', '2022-06-26 14:41:09', 'cn'),(5, '公司简介', '公司简介', 1, 0, '2022-09-13 01:25:11', '2022-09-13 01:25:11', '2022-06-29 01:29:34', 'cn');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `k-cms`.`k_link` WRITE;
DELETE FROM `k-cms`.`k_link`;
INSERT INTO `k-cms`.`k_link` (`id`,`title`,`description`,`url`,`img`,`sort`,`state`,`delete_time`,`update_time`,`create_time`) VALUES (1, '123123', '23123123', '', '', 1, 2, NULL, '2022-06-26 18:11:19', '2022-06-26 18:00:17');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `k-cms`.`k_site_config` WRITE;
DELETE FROM `k-cms`.`k_site_config`;
INSERT INTO `k-cms`.`k_site_config` (`id`,`site_title`,`keywords`,`description`,`copyright`,`icp`,`tel`,`mobile`,`hot_tel`,`qq`,`weixin`,`github`,`gitee`,`email`,`fax`,`address`,`logo`,`code2`,`script_code`,`smtp_host`,`smtp_port`,`email_account`,`email_pwd`,`email_recipient`,`update_time`,`create_time`,`delete_time`,`page_list_rows`,`theme`) VALUES (1, '程斌个人博客系统', '程斌,博客,个人博客,博客系统,k_blog,blog系统,18php', '这是程斌的个人博客，是由程斌自主开发的k_blog个人博客系统构建，主要记录php、linux、前端、vue、docker、python、golang、swoole等互联网开发技术。', 'Copyright @ 2020 18php.com', '粤ICP备15100508号1', '18819009046', '', '', '450038893', '', 'https://github.com/kitty168', 'https://gitee.com/kitty_18php', '450038893@qq.com', '', '', '/storage/images/20200416/ac596aa7400814e04167bbee3b82e6c8.png', '/storage/images/20200416/b43989af915b78bbf0d142323b38cfe6.jpg', '<script>\n    var _hmt = _hmt || [];\n    (function() {\n        var hm = document.createElement(\"script\");\n        hm.src = \"https://hm.baidu.com/hm.js?4b8b0f001adf63699db8dc08f5259cc4\";\n        var s = document.getElementsByTagName(\"script\")[0];\n        s.parentNode.insertBefore(hm, s);\n    })();\n</script>', '', '', 'admin', 'admin', '', '2022-06-18 00:00:00', NULL, NULL, 0, 'default');
UNLOCK TABLES;
COMMIT;
