<?php
namespace app\index\controller;


use think\App;
use app\common\model\Article as ArticleModel;

class Index extends Base
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    public function index()
    {
        $articleModel = new ArticleModel;
        $productList = $articleModel->getListAll([
            ['article_cate_id','=',1],
            ['state','=',1]
        ],true,'sort');

        $companyInfo = $articleModel->getInfo([
            ['article_cate_id', '=',5],
            ['state', '=', 1]
        ],true,'sort');


        $newsList = $articleModel->getListPage(5,[
            ['article_cate_id', '=',2],
            ['state', '=', 1]
        ],true,'sort');


        return $this->fetch('',compact(['productList','companyInfo','newsList']));
    }

}
