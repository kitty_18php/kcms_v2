<?php
namespace app\index\controller;


use app\common\model\ArticleCate as ArticleCateModel;
use app\common\model\Article as ArticleModel;
use think\App;

class Article extends Base
{
    protected $model;
    protected $articleCateModel;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new ArticleModel;
        $this->articleCateModel = new ArticleCateModel;
    }

    public function index($id)
    {

        if($id == 5||$id==4) {
            $info = $this->model->getInfo([
                ['article_cate_id','=',$id],
                ['lang_tag','=',$this->lang],
            ],true,'',['article_cate']);
            $this->assign('info',$info);
            $template = 'detail';

            return $this->fetch($template);
        }

        $articleCateInfo = $this->articleCateModel->getInfo($id);
        $this->assign('articleCateInfo',$articleCateInfo);

        $map = [];
        $map[] = ['article_cate_id','=',$id];
        $list = $this->model->getListPage(10,$map,true, 'sort');
        $template = '';


        $this->assign('list',$list);

        return $this->fetch($template);
    }

    public function detail($id)
    {
        $info = $this->model->getInfo($id,true,'',['article_cate']);
        $this->assign('info',$info);
        return $this->fetch();
    }


}
