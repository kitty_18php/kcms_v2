<?php
/**
 *
 * User: kitty.cheng
 * Mail: 450038893@qq.com
 * Date: 2022/6/26
 * Time: 11:32
 * Created by. 九州大健康
 */

use think\facade\Route;

Route::group('api',function(){
    Route::get('/', 'api/Index/index');
});