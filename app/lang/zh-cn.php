<?php
/**
 *
 * User: kitty.cheng
 * Mail: 450038893@qq.com
 * Date: 2022/6/26
 * Time: 15:35
 * Created by. 九州大健康
 */

return [
    // 系统
    'add'       => '添加',
    'edit'      => '编辑',
    'delete'    => '删除',
    'update'    => '更新',
    'restore'   => '恢复',
    'submit'    => '提交',
    'success'   => '成功',
    'error'     => '错误',
    'fail'      => '失败',
    'list'      => '列表',
    'commit'    => '提交',
    'save'      => '保存',
    'set'       => '设置',
    'reset'     => '重置',
    'operation' => '操作',
    'refresh'   => '刷新',

    'welcome' => '欢迎',
    'login'   => '登录',
    'logout'  => '退出',

    'site_name' => '站点名称',
    'title'     => '标题',
    'describe'  => '描述',
    'description'  => '描述',
    'version'   => '版本',

    // 常用
    'mobile'    => '手机号',
    'address'   => '地址',
    'state'     => '状态',
    'account'   => '账号',
    'password'  => '密码',

    'sort'  => '排序',

    'home'      => '首页',
    'dashboard' => '仪表盘',

    'create_time' => '创建时间',
    'update_time' => '更新时间',
    'delete_time' => '删除时间',

    'preview'           => '预览',
    'total'             => '合计',

    // input
    'please_input'      => '请输入',
    'input_keyword'      => '请输入关键词',
    'create_date'       => '创建日期',
    'date'              => '日期',
    'switch_on'         => '启用',
    'switch_off'        => '禁用',

    // 业务
    'name' => '名称',
    'cate_name' => '分类名称',
    'cloud_park_system' => '云端停车场系统',
    'tenant'            => '商户',
    'system'            => '系统',
    'foreground'        => '前台',
    'background'        => '后台',
    'tenant_login'      => '商户登录',
    'manager_login'     => '管理员登录',

    'park'                  => '车场',
    'gate'                  => '道口',
    'park_lock'             => '车位锁',
    'car_plate_recognition' => '车牌识别',
    'paymach'               => '缴费机',
    'tenant_name'           => '商户名称',
    'parent_tenant_name'    => '父级商户名称',

    'park_name'     => '车场名称',

    // 左侧菜单
    'menu'          => '菜单',
    'tenant_manage' => '商户管理',
    'tenant_list'   => '商户列表',

    'park_manage'    => '停车场管理',
    'park_list'      => '停车场列表',
    'gate_list'      => '道口列表',
    'park_lock_list' => '车位锁列表',
    'paymach_list'   => '缴费机列表',

    'record_query'                => '记录查询',
    'car_in_record'               => '车辆入场记录',
    'car_out_record'              => '车辆出场记录',
    'car_number_payment_record'   => '车牌缴费记录',
    'park_lock_payment_record'    => '车位锁缴费记录',
    'paymach_changeshifts_record' => '缴费机交班记录',

    'system_manage'        => '系统管理',
    'adminer_list'         => '管理员列表',

    // input placeholder
    'input_tenant_name'    => '请输入商户名称',
    'select_tenant'        => '请选择商户',
    'select_park'          => '请选择车场',
    'top_level'            => '顶级',
    'input_park_name'      => '请输入车场名称',
    'input_gate_name'      => '请输入道口名称',
    'input_car_number'     => '请输入车牌号码',
    'payment_date'         => '缴费日期',
    'input_account'        => '请输入账号名',
    'input_paymach_no'     => '请输入缴费机编号',
    'start_date'           => '开始日期',
    'over_date'            => '截止日期',

    // -------------------
    // data table header
    // -------------------

    // ParkLock List
    'park_cell'            => '车格号码',
    'connect_state'        => '连接状态',
    'has_car'              => '是否有车',
    'in_time'              => '入位时间',
    'is_pay'               => '是否缴费',
    'pay_amount'           => '缴费金额',
    'pay_time'             => '缴费时间',
    'lock_status'          => '锁状态',
    'force_up'             => '强升',
    'force_down'           => '强降',
    'down'                 => '下降',

    // tenant field
    'tenant_key'           => '商户密钥',

    // tenant update field tips
    'tenant_password_tips' => '6-16个字符',

    // park list
    'park_type'            => '车场类型',
    'total_plot'           => '车位总数',
    'empty_plot'           => '空闲车位数',

    // gate list
    'gate_no'              => '道口编号',
    'gate_name'            => '道口名称',
    'park_open'            => '开闸',

    // paymach list
    'invoice_reset'        => '发票清零',
    'bill_100'             => '￥100',
    'coin_50'              => '￥50',
    'coin_10'              => '￥10',
    'coin_5'               => '￥5',
    'invoice_count'        => '发票数',
    'paymach_no'           => '缴费机编号',
    'paymach_name'         => '缴费机名称',
    'online_status'        => '在线状态',
    'online'               => '在线',
    'offline'              => '离线',

    // Car in list
    'user_type'            => '用户类型',
    'car_type'             => '车型',
    'car_number'           => '车牌号码',

    'park_in_time'    => '入场时间',
    'park_in_gate_no' => '入场通道编号',
    'park_in_status'  => '入场状态',
    'park_in_picture' => '入场照片',

    'park_out_time'    => '出场时间',
    'park_out_gate_no' => '出场通道编号',
    'park_out_status'  => '出场状态',
    'park_out_picture' => '出场照片',

    //  ParkLock Pay Records  payment_list
    'qrcode_freetime'  => '小票免费时间(小时)',
    'cash'             => '现金金额',
    'easy_card'        => '悠游卡扣费',
    'change_value'     => '找零金额',
    'total_value'      => '总需缴费金额',

    // paymach_changeshifts list
    'shift_time'       => '交班时间',
    'total_money'      => '总金额',

    // api common msg
    'add_success'      => '添加成功',
    'update_success'   => '更新成功',
    'delete_success'   => '删除成功',
    'save_success'     => '保存成功',
    'handle_success'   => '操作成功',
    'login_success'    => '登录成功',

    'add_fail'    => '添加失败',
    'update_fail' => '更新失败',
    'delete_fail' => '删除失败',
    'save_fail'   => '保存失败',
    'handle_fail' => '操作失败',
    'login_fail'  => '登录失败',

    'bad_request'   => '错误的请求',
    'data_not_find' => '数据没有找到',

    'params_error'      => '参数错误',

    // 错误提示
    'admin_error_msg_1' => '您不能更新超级管理员的信息',
    'admin_error_msg_2' => '超级管理员不能被删除',
    'admin_error_msg_3' => '超级管理员不允许被禁用',

    'login_error_msg_1' => '登录名或密码不能为空',
    'login_error_msg_2' => '用户名或密码错误',

    'login_timeout'        => '登录超时，请点击跳转',
    'park_error_msg_1'     => '空闲车位不能大于总车位数',


    // -----------------------------------
    // validate msg
    // -----------------------------------

    // gate validate
    'tenant_id_require'    => 'tenant_id必须',
    'tenant_id_number'     => 'tenant_id格式错误',
    'park_id_require'      => 'park_id必须',
    'park_id_number'       => 'park_id格式错误',
    'gate_no_require'      => '道口编号必须填写',
    'gate_no_length'       => '道口编号长度不符，2-50',
    'gate_name_require'    => '道口名称必须填写',
    'gate_name_length'     => '道口名称长度不符，2-50',

    // park_lock validate
    'park_cell_require'    => '车格必须填写',
    'park_cell_length'     => '车格长度不符，2-50',

    // paymach validate
    'paymach_no_require'   => '缴费机编号必须填写',
    'paymach_no_length'    => '缴费机编号长度不符，2-50',
    'paymach_name_require' => '缴费机名称必须填写',
    'paymach_name_length'  => '缴费机名称长度不符，2-50',

    // tenant validate
    'tenant_name_require'  => '商户名称必须',
    'tenant_name_length'   => '商户名称长度不符，2-50',
    'account_require'      => '账号必须',
    'account_length'       => '账号长度不符，4-16',
    'account_unique'       => '账号已存在',
    'password_length'      => '密码长度不符，6-16',
    'mobile_mobile'        => '手机号格式错误',
    'address_length'       => '地址长度不符，6-50',

    // admin validate
    'admin_name_require'   => '账号必须',
    'admin_name_length'    => '账号长度不符，4-16',
    'admin_name_unique'    => '账号已存在',
    'password_require'     => '密码必须',
    'mobile_require'       => '手机号必须',
    'email_require'        => '邮箱必须',
    'email_email'          => '邮箱格式错误',


];