<?php
/**
 *
 * User: kitty.cheng
 * Mail: 450038893@qq.com
 * Date: 2022/6/26
 * Time: 13:40
 * Created by. 九州大健康
 */

namespace app\common\traits;


use think\Exception;
use think\exception\ValidateException;
use think\facade\Config;

trait Curd
{
    // 控制器简单描述名称
    // protected $remarkName='';

    /**
     * 获取列表数据
     * @return mixed
     */
    public function index()
    {
        if($this->request->isAjax()) {
            return $this->getList();
        }
        return $this->fetch();
    }

    /**
     * 列表数据接口
     * @return mixed
     */
    public function getList()
    {
        $pageNum = input('get.pageNum/d',1);
        $pageSize = input('get.pageSize/d',Config::get('pageConfig.size',10));
        $map = $this->buildMap();
        $with = $this->buildWith();
        $field = true;
        $order = 'id desc';
        $list = [];
        $count = $this->model->getCount($map);

        $count && $list = $this->model->getList($pageNum,$pageSize,$map,$field,$order,$with);
        $responseData = [
            'list' => $list,
            'count' => $count,
            // 'sql' => $this->model->getLastSql()
        ];
        return $this->apiReturn(200,$this->remarkName.'数据读取成功',$responseData);
    }

    /**
     * 构建查询条件map
     * @return array
     */
    protected function buildMap()
    {
        $create_time = input('get.create_time/s','', 'trim');
        $keyword_field = input('get.keyword_field/s','','trim');
        $keyword = input('get.keyword/s','','trim');

        $map = [];
        if($create_time){
            list($s_date,$e_date) = explode(' - ',$create_time);
            $map[] = ['create_time','between',[$s_date . ' 00:00:00',$e_date . ' 23:59:59']];
        }

        if($keyword_field && $keyword) {
            $map[] = [$keyword_field,'like',"%$keyword%"];
        }


        return $map;
    }

    /**
     * 关联模式
     * @return array
     */
    protected function buildWith()
    {
        return [];
    }

    /**
     * 添加
     * @return mixed
     * @throws \ReflectionException
     */
    public function add()
    {
        if($this->request->isPost()) {
            $data = input('post.');

            try {
                $this->validate($data, get_validate_class($this->model));
            }catch (ValidateException $e) {
                return $this->apiReturn(1002,$e->getMessage());
            }

            try{
                $res = $this->model->save($data);
            }catch (Exception $e) {
                return $this->apiReturn(1003,$this->remarkName.'添加失败');
            }

            return $res ?
                $this->apiReturn(200, $this->remarkName.'添加成功') :
                $this->apiReturn(1004,$this->remarkName.'添加失败');
        }else {
            return $this->fetch();
        }

    }

    /**
     * 修改
     * @return mixed
     * @throws \ReflectionException
     */
    public function edit()
    {
        if($this->request->isPost()) {
            $data = $this->request->post();

            try {
                $this->validate($data, get_validate_class($this->model));
            }catch (ValidateException $e) {
                return $this->apiReturn(1002,$e->getMessage());
            }

            $id = $this->request->post('id/d',0);
            $info = $this->model->getInfo($id);
            if(!$info) return $this->apiReturn(1001,$this->remarkName.'数据不存在');

            try{
                $res = $info->save($data);
            }catch (Exception $e) {
                return $this->apiReturn(1001,$this->remarkName.'更新失败');
            }

            return $res ?
                $this->apiReturn(200,$this->remarkName.'数据更新成功') :
                $this->apiReturn(1001, $this->remarkName.'没有数据被更新');
        }else{
            $id = input('id/d',0);
            $info = $this->model->getInfo($id);

            if(!$info) return $this->error('数据不存在');
            $this->assign('info',$info);
            return $this->fetch();
        }

    }

    /**
     * 逻辑删除
     * @return mixed
     */
    public function delete()
    {
        if($this->request->isPost()){
            $id = input('post.id',0);
            $info = $this->model->getInfo($id);

            if(!$info) return $this->apiReturn(1001,$this->remarkName.'数据不存在');
            try{
                $res = $info->delete();
            }catch (Exception $e) {
                return $this->apiReturn(1001,$this->remarkName.'删除失败');
            }

            return $res ?
                $this->apiReturn(200, $this->remarkName.'删除成功') :
                $this->apiReturn(1001,$this->remarkName.'删除失败');
        }

        return $this->apiReturn(1001, '非法请求');
    }

    /**
     * 局部更新
     * @return mixed
     */
    public function modify()
    {
        $id = input('post.id/d',0,'trim');
        $info = $this->model->getInfo($id);

        if (!$info) return $this->apiReturn(1001,$this->remarkName.'数据没有找到');

        $attributes = $this->request->only($this->model->getAllowModifyField());

        if (!$attributes) return $this->apiReturn(1001,$this->remarkName.'没有可更新的内容');

        $this->model->startTrans();
        try{

            $res = $info->save($attributes);

            $this->model->commit();
        }catch (Exception $e) {
            $this->model->rollback();
            return $this->apiReturn(2001, $this->remarkName.'更新失败');
        }

        return $res ?
            $this->apiReturn(200, $this->remarkName.'更新成功') :
            $this->apiReturn(1001,$this->remarkName.'更新失败');
    }

}