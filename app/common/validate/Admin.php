<?php

namespace app\common\validate;

use think\Validate;

class Admin extends Validate
{
    protected $rule =   [
        'user_name|用户名' => 'require|unique:admin',
        'mobile|手机号' => ['require','mobile'],
        'email|邮箱' => ['require','email'],
    ];

    protected $message  =   [
        // 'mobile.mobile' => '手机号格式错误'
    ];

    protected $scene = [

    ];
}