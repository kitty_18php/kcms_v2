<?php
namespace app\common\model;

use think\facade\Config;
use think\Model;

class BaseModel extends Model
{
    // 允许更新的字段
    protected $allowModifyField = [];

    /**
     * 获取允许更新的字段
     * @return array
     */
    public function getAllowModifyField()
    {
        return $this->allowModifyField;
    }

    /**
     * 获取列表数据集合
     * @param int $pageNum
     * @param int $pageSize
     * @param array $map
     * @param bool $field
     * @param string $order
     * @param array $with
     * @param bool $cache
     * @param mixed ...$args
     * @return mixed
     */
    public function getList($pageNum = 1, $pageSize = 10, $map = [], $field = true, $order = 'id desc', $with = [], $cache = false, ...$args)
    {
        $action = $this->execField($field);
        return $this->$action($field)->with($with)->where($map)->page($pageNum, $pageSize)->order($order)->cache($cache)->select();
    }

    /**
     * 获取top数据列表集合
     * @param int $limit
     * @param array $map
     * @param bool $field
     * @param string $order
     * @param array $with
     * @param bool $cache
     * @param mixed ...$args
     * @return mixed
     */
    public function getListTop($limit = 10, $map = [], $field = true, $order = '', $with = [], $cache = false, ...$args)
    {
        $action = $this->execField($field);
        return $this->$action($field)->with($with)->where($map)->order($order)->limit($limit)->cache($cache)->select();
    }


    /**
     * 获取带分页数据的集合
     * @param int $pageRows
     * @param array $map
     * @param bool $field
     * @param string $order
     * @param array $with
     * @param bool $cache
     * @param mixed ...$args
     * @return mixed
     */
    public function getListPage($pageRows = 10, $map = [], $field = true, $order = 'id desc', $with = [], $cache = false, ...$args)
    {
        $action = $this->execField($field);
        return $this->$action($field)->with($with)->where($map)->order($order)->cache($cache)->paginate($pageRows);
    }

    /**
     * 获取所有列表数据集合
     * @param array $map
     * @param bool $field
     * @param string $order
     * @param array $with
     * @param bool $cache
     * @param mixed ...$args
     * @return mixed
     */
    public function getListAll($map = [], $field = true, $order = '', $with = [], $cache = false, ...$args)
    {
        $action = $this->execField($field);
        return $this->$action($field)->with($with)->where($map)->order($order)->cache($cache)->select();
    }


    /**
     * 获取单条详情数据
     * @param array $map
     * @param bool $field
     * @param array $order
     * @param array $with
     * @param mixed ...$args
     * @return mixed
     */
    public function getInfo($map = [], $field = true, $order = [], $with = [], ...$args) {
        $action = $this->execField($field);
        if(is_numeric($map)) {
            return $this->$action($field)->with($with)->find($map);
        }

        return $this->$action($field)->with($with)->where($map)->find();
    }


    /**
     * getInfo 别名
     * @param array $map
     * @param bool $field
     * @param array $order
     * @param array $with
     * @param mixed ...$args
     * @return mixed
     */
    public function getDetail($map = [] ,$field = true, $order = [], $with = [], ...$args)
    {
        return $this->getInfo($map,$field,$order,$with,...$args);
    }

    /**
     * 获取总条数
     * @param $map
     * @param mixed ...$args
     * @return int
     */
    public function getCount($map, ...$args)
    {
        return $this->where($map)->count();
    }

    /**
     * getCount别名
     * @param $map
     * @param mixed ...$args
     * @return int
     */
    public function getTotal($map, ...$args)
    {
        return $this->getCount($map,...$args);
    }

    /**
     * field输出字段处理
     * @param $field
     * @return string
     */
    protected function execField(&$field)
    {
        $action = 'field';
        if (isset($field['_except'])) {
            unset($field['_except']);
            $action = 'withoutField';
        }
        return $action;
    }

}