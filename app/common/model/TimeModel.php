<?php
/**
 *
 * User: kitty.cheng
 * Mail: 450038893@qq.com
 * Date: 2022/6/26
 * Time: 14:07
 * Created by. 九州大健康
 */

namespace app\common\model;


use think\model\concern\SoftDelete;

class TimeModel extends BaseModel
{
    use SoftDelete;
    protected $autoWriteTimestamp = true;

    protected $deleteTime = 'delete_time';

}