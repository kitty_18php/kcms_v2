<?php
/**
 * Name:
 * User: Kitty.Cheng
 * Mail: kitty.cheng@18php.com
 * Date: 2020/3/22
 * Time: 上午1:34
 * Created by 18php.com
 */

namespace app\common\model;


class Company extends TimeModel
{
    protected $allowModifyField = [
        'state'
    ];

    public function companyCate()
    {
        return $this->belongsTo(CompanyCate::class)->bind(['cate_name']);
    }


}