<?php
/**
 * Name:
 * User: Kitty.Cheng
 * Mail: kitty.cheng@18php.com
 * Date: 2020/3/22
 * Time: 上午1:34
 * Created by 18php.com
 */

namespace app\common\model;


class Article extends TimeModel
{
    protected $allowModifyField = [
        'state'
    ];

    /**
     * @return \think\model\relation\BelongsTo
     */
    public function articleCate()
    {
        return $this->belongsTo(ArticleCate::class)->bind(['cate_name']);
    }

}