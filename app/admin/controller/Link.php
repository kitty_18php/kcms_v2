<?php

/**
 * Name:
 * User: Kitty.Cheng
 * Mail: kitty.cheng@18php.com
 * Date: 2020/3/22
 * Time: 上午1:27
 * Created by 18php.com
 */

namespace app\admin\controller;


use app\common\model\Link as LinkModel;
use app\common\traits\Curd;
use think\App;

class Link extends Base
{
    use Curd;

    protected $remarkName = '友情链接';

    protected $model;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new LinkModel;
    }


}