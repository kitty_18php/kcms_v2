<?php
namespace app\admin\controller;

use app\common\controller\BaseController;
use think\App;
use think\facade\Config;

class Base extends BaseController
{

    protected $middleware = ['checkLogin'];

    protected $currentUser;
    protected $lang;

    public function __construct(App $app)
    {
        parent::__construct($app);

    }

    protected function initialize()
    {
        parent::initialize();
        $this->currentUser = session('adminInfo');
        // $modelFullName = '\\app\\common\model\\'.$this->request->controller();
        // class_exists($modelFullName) && $this->model = new $modelFullName();
        // dump($admin_info);
        $lang = Config::get('sys.web_lang')[Config::get('sys.default_lang')];
        $this->lang = $lang;
        $this->assign('lang',$lang);
    }

    public function __call($name, $arguments)
    {
        return 'base error:'.$name;
    }

}