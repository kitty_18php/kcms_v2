<?php
/**
 * Name:
 * User: Kitty.Cheng
 * Mail: kitty.cheng@18php.com
 * Date: 2020/3/22
 * Time: 上午1:27
 * Created by 18php.com
 */

namespace app\admin\controller;


use app\common\traits\Curd;
use app\common\model\CompanyCate as CompanyCateModel;
use think\App;
use think\Exception;
use think\exception\ValidateException;
use think\facade\Config;

class CompanyCate extends Base
{
    use Curd;

    protected $remarkName = '公司信息';

    protected $model;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new CompanyCateModel;
    }

}