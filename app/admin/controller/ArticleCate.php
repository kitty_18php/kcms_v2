<?php
/**
 * Name:
 * User: Kitty.Cheng
 * Mail: kitty.cheng@18php.com
 * Date: 2020/3/22
 * Time: 上午1:27
 * Created by 18php.com
 */

namespace app\admin\controller;


use app\common\model\ArticleCate as ArticleCateModel;
use app\common\traits\Curd;
use think\App;

class ArticleCate extends Base
{
    use Curd;

    protected $remarkName = '文章分类';

    protected $model;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new ArticleCateModel;
    }


}