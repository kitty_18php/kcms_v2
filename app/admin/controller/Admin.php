<?php
/**
 * Name: 管理员控制器
 * User: Kitty.Cheng
 * Mail: kitty.cheng@18php.com
 * Date: 2020/3/20
 * Time: 下午3:15
 * Created by 18php.com
 */

namespace app\admin\controller;


use app\common\traits\Curd;
use think\App;
use think\Exception;
use think\exception\ValidateException;

class Admin extends Base
{
    use Curd;
    protected $remarkName='管理员';
    protected $model;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new \app\common\model\Admin;
    }

    public function add()
    {
        if($this->request->isPost()) {
            $data = input('post.');

            try{
                $this->validate($data, get_validate_class($this->model));
                $res = $this->model->save($data);
            }catch (ValidateException $e) {
                return $this->apiReturn(1000,$e->getMessage());
            }catch (Exception $e) {
                return $this->apiReturn(0,$e->getMessage());
            }

            return $res ?
                $this->apiReturn(200, $this->remarkName.'添加成功') :
                $this->apiReturn(0,$this->remarkName.'添加失败');
        }else {
            return $this->fetch();
        }
    }

    public function edit()
    {
        if($this->request->isPost()) {
            $data = $this->request->post();

            try {
                $this->validate($data, get_validate_class($this->model));
            }catch (ValidateException $e) {
                return $this->apiReturn(1002,$e->getMessage());
            }

            $id = $this->request->post('id/d',0);
            $info = $this->model->getInfo($id);
            if(!$info) return $this->apiReturn(1001,$this->remarkName.'数据不存在');

            try{
                if($data['password']) {
                    $data['password'] = md5($data['password']);
                }else{
                    unset($data['password']);
                }
                $res = $info->save($data);
            }catch (Exception $e) {
                return $this->apiReturn(1001,$this->remarkName.'更新失败');
            }

            return $res ?
                $this->apiReturn(200,$this->remarkName.'数据更新成功') :
                $this->apiReturn(1001, $this->remarkName.'没有数据被更新');
        }else{
            $id = input('id/d',0);
            $info = $this->model->getInfo($id);

            $this->assign('info',$info);
            return $this->fetch();
        }
    }


}