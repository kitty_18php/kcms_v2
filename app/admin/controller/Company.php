<?php
/**
 * Name:
 * User: Kitty.Cheng
 * Mail: kitty.cheng@18php.com
 * Date: 2020/3/22
 * Time: 上午1:27
 * Created by 18php.com
 */

namespace app\admin\controller;


use app\common\traits\Curd;
use app\common\model\Company as CompanyModel;
use think\App;
use think\Exception;
use think\exception\ValidateException;
use think\facade\Config;

class Company extends Base
{
    use Curd;

    protected $remarkName = '公司信息';

    protected $model;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new CompanyModel;
    }

    public function getList()
    {
        $pageNum = input('get.pageNum/d',1);
        $pageSize = input('get.pageSize/d',Config::get('pageConfig.size',10));
        $map = $this->buildMap();

        $lang_tag = input('get.lang_tag/s','');
        $lang_tag && $map[] = ['lang_tag', '=', $lang_tag];

        $with = $this->buildWith();
        $field = true;
        $order = 'id desc';
        $list = [];
        $count = $this->model->getCount($map);

        $count && $list = $this->model->getList($pageNum,$pageSize,$map,$field,$order,$with);
        $responseData = [
            'list' => $list,
            'count' => $count,
            // 'sql' => $this->model->getLastSql()
        ];
        return $this->apiReturn(200,$this->remarkName.'数据读取成功',$responseData);
    }

    protected function buildWith()
    {
        return ['company_cate'];
    }

    /**
     * @return string|\think\response\Json
     * @throws \Exception
     */
    public function add(){

        if($this->request->isPost()){
            $data = $this->request->post();

            try {
                $this->validate($data,get_validate_class($this->model));
            }catch (ValidateException $e) {
                return $this->apiReturn(1001,$e->getMessage());
            }

            try{
                $res = $this->model->save($data);
            }catch (Exception $e) {
                return $this->apiReturn(1003,$this->remarkName.'添加失败');
            }

            return $res ?
                $this->apiReturn(200, $this->remarkName.'添加成功') :
                $this->apiReturn(1004,$this->remarkName.'添加失败');
        }else{
            $cateMap[] = ['state','=',2];
            $articleCateList = $this->articleCateModel->getListAll($cateMap);
            $this->assign('articleCateList',$articleCateList);
            return $this->fetch();
        }

    }

    /**
     * 修改
     * @return mixed
     * @throws \ReflectionException
     */
    public function edit()
    {
        if($this->request->isPost()) {
            $data = $this->request->post();

            try {
                $this->validate($data, get_validate_class($this->model));
            }catch (ValidateException $e) {
                return $this->apiReturn(1002,$e->getMessage());
            }

            $id = $this->request->post('id/d',0);
            $info = $this->model->getInfo($id);
            if(!$info) return $this->apiReturn(1001,$this->remarkName.'数据不存在');

            try{
                $res = $info->save($data);
            }catch (Exception $e) {
                return $this->apiReturn(1001,$this->remarkName.'更新失败');
            }

            return $res ?
                $this->apiReturn(200,$this->remarkName.'数据更新成功') :
                $this->apiReturn(1001, $this->remarkName.'没有数据被更新');
        }else{
            $id = input('id/d',0);

            $info = $this->model->getInfo($id);

            $articleCateList= $this->articleCateModel->getListAll([['state','=',2]]);

            $this->assign('info',$info);
            $this->assign('articleCateList',$articleCateList);
            return $this->fetch();
        }

    }

}