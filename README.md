KCMS 2.0
===============

> 运行环境要求PHP7.1+  
> 基于ThinkPHP 6.x

## 主要功能

* 开箱即用的cms内容管理系统
* 可作为基本的后台系统的基础框架
* 封装常用的Model方法
* 采用传统的mvc模式
* 权限管理
* 代码生成器

## 安装

下载:
~~~
git clone https://gitee.com/kitty_18php/kcms_v2.git kcms
~~~

安装:
~~~
composer install
~~~

## 文档

[完全开发手册](https://www.kancloud.cn/manual/thinkphp6_0/content)

## 技术交流

QQ群：576599686 [k_cms]

## 版权信息

CMS遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2020-2022 by 18PHP (http://www.18php.com)

All rights reserved。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
